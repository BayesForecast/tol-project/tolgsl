/* CppTools.cpp: C++ API to invoke GSL from TOL

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */


//Starts local namebock scope
#define LOCAL_NAMEBLOCK _local_namebtntLock_
#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_bmatgra.h>
#include <tol/tol_bsetgra.h>

#include "gsl/gsl_errno.h"
#include "TolGslEigen.h"

#define dMat(arg) ((DMat&)Mat(arg))

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& _local_namebtntLock_ = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function
DynAPI void* GetDynLibNameBlockTolGsl()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

//--------------------------------------------------------------------
DeclareContensClass(BSet, BSetTemporary, BSetGslEigenNonSymmetric);
DefMethod(1, BSetGslEigenNonSymmetric, "EigenNonSymmetric", 1, 1,
  "Matrix",
  "(Matrix A)",
  "This function computes the eigenvalues of the real nonsymmetric matrix A."
  "The result is a Set with two elements one with the real part, "
  "the other with the imaginary part",
  BOperClassify::NumericalAnalysis_);
//--------------------------------------------------------------------
void BSetGslEigenNonSymmetric::CalcContens()
{
  BMatrix<BDat>& A = Mat(Arg(1));
  BMatrix<BDat> realPart, imagPart;
  int status = TolGsl_Eigen_NonSymmetric(A, realPart, imagPart);
  if (status == GSL_SUCCESS)
    {
    BUserMat* eigenReal = BContensMat::New("", realPart, "eigen real part");
    eigenReal->PutName( "real" );
    BUserMat* eigenImag = BContensMat::New("", imagPart, "eigen imaginary part");
    eigenImag->PutName( "imag" );
    BList* lst  = NCons(eigenImag);
    contens_.RobElement(Cons(eigenReal, lst));
    }
  else
    {
    Error(gsl_strerror(status));
    }
}
